﻿using System;
using System.IO;
using System.Collections.Generic;
using Microsoft.VisualBasic.FileIO;
using System.Linq;



namespace MyExam
{
    public class Manager {
        public string Name {get; set; }
        public int Proceeds {get; set; }
    }

    public class Order {
        public int Month {get; set; }
        public int Year {get; set; }

        public string Manager {get; set; }
        public int Price {get; set;}
    }

    public class ReportString {
        public int Month {get; set; }
        public int Year {get; set; }

        public int TotalProceeds {get; set; }

        public string BestManager {get; set; }
        public int BestManagerProceeds {get; set; }

        public string WorstManager {get; set; }
        public int WorstManagerProceeds {get; set; }
        
    }

    public class Report {
        public List<Order> Orders = new List<Order>();
        private List<ReportString> strings = new List<ReportString>();
        public void Generate() {
            this.Orders.ForEach(delegate(Order orderExample) {
                if (!this.strings.Exists(x => x.Month == orderExample.Month && x.Year == orderExample.Year)) {
                    ReportString newString = new ReportString();
                    newString.Year = orderExample.Year;
                    newString.Month = orderExample.Month;
                    newString.TotalProceeds = orderExample.Price;
                    this.strings.Add(newString);
                } else {
                    ReportString existingString = this.strings.Find(x => x.Month == orderExample.Month && x.Year == orderExample.Year);
                    existingString.TotalProceeds+= orderExample.Price;
                }
            });
            
            this.strings.ForEach(delegate(ReportString str) {
                List<Manager> managers = new List<Manager>();
                this.Orders.FindAll(x => x.Year == str.Year && x.Month == str.Month).ForEach(delegate(Order order) {
                    if (!managers.Exists(x => x.Name == order.Manager)) {
                        Manager newManager = new Manager();
                        newManager.Name = order.Manager;
                        newManager.Proceeds = order.Price;

                        managers.Add(newManager);
                    } else {
                        Manager existingManager = managers.Find(x => x.Name == order.Manager);
                        existingManager.Proceeds += order.Price;
                    }

                });

                managers.Sort(delegate(Manager x, Manager y) {
                    return x.Proceeds.CompareTo(y.Proceeds);
                });

                Manager bestManager = managers.Last();

                str.BestManager = bestManager.Name;
                str.BestManagerProceeds = bestManager.Proceeds;

                Manager worstManager = managers.First();

                str.WorstManager = worstManager.Name;
                str.WorstManagerProceeds = worstManager.Proceeds;
            });
        }


        public void WriteToFile() {
            string path = Path.GetFullPath("./files/output.csv");

            string createText = "Год;Месяц;Выручка;Лучший продавец;Выручка лучшего продавца;Худший продавец;Выручка худшего продавца" + Environment.NewLine;
            File.WriteAllText(path, createText);
            

            this.strings.ForEach(delegate(ReportString str){
                string month = "";
                switch (str.Month) {
                    case 1:
                    month = "Январь";
                    break;
                    case 2:
                    month = "Февраль";
                    break;
                    case 3:
                    month = "Март";
                    break;
                    case 4:
                    month = "Апрель";
                    break;
                    case 5:
                    month = "Май";
                    break;
                    case 6:
                    month = "Июнь";
                    break;
                    case 7:
                    month = "Июль";
                    break;
                    case 8:
                    month = "Август";
                    break;
                    case 9:
                    month = "Сентябрь";
                    break;
                    case 10:
                    month = "Октябрь";
                    break;
                    case 11:
                    month = "Ноябрь";
                    break;
                    case 12:
                    month = "Декабрь";
                    break;
                }
                File.AppendAllText(path, $"{str.Year};{month};{str.TotalProceeds};{str.BestManager};{str.BestManagerProceeds};{str.WorstManager};{str.WorstManagerProceeds}"+ Environment.NewLine);
            });

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Report myReport = new Report();
            using (TextFieldParser parser = new TextFieldParser(Path.GetFullPath("./files/input.csv")))
        {
            parser.Delimiters = new string[] { ";" };
            int index = 0;
            while (true)
            {
                string[] parts = parser.ReadFields();
                if (parts == null)
                {
                    break;
                }

                if (index > 0) {

                

                // parts[1] - дата заказа
                // parts[2] — менеджер
                // parts[5] — сумма заказа
                Order order = new Order();
                order.Manager = parts[2];
                order.Price = int.Parse(parts[5]);
                order.Month = int.Parse(parts[1].Split('.')[1]);
                order.Year = int.Parse(parts[1].Split('.')[2]);
                
                myReport.Orders.Add(order);
                }
                index++;

            }

            myReport.Generate();
            myReport.WriteToFile();
        }
        }
    }
}
